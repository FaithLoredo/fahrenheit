package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTesting {

	@Test
	public final void testIn() {
		assertTrue("Did not convert to Fahrenheit",Fahrenheit.convertFromCelsius(20)==52); // TODO
	}
	
	@Test
	public final void testOut() {
		assertFalse("Not valid input",Fahrenheit.convertFromCelsius(20)==32); // TODO
	}

	@Test
	public final void testBoundIn() {
		assertTrue("Did not convert to Fahrenheit",Fahrenheit.convertFromCelsius((int)20.8)==52);
	}
	
	@Test
	public final void testBoundOut() {
		assertFalse("Logic error ",Fahrenheit.convertFromCelsius((int)20.8)==53);
	}
}
